// Only addition, subtraction, multiplication and division operations
var firstNumber;
var operator;
var secondNumber;
var resultDisplayed;

//displays the numbers clicked
function appendNumber(num) {
	var displayedValue = document.getElementById('result').value;
	//if the displayed value is zero then it is replaced by the number entered, 
	//if a result is displayed then immediate click on a number will over write the result with the number
	if (displayedValue == "0" || resultDisplayed == true)
		document.getElementById('result').value = num;
	//appending the number
	else
		document.getElementById('result').value += num;
	resultDisplayed = false;
}

//clears the displayed value and initialises it to zero.
function clearAll() {
	document.getElementById('result').value = "0";
}

//appends operator, performs the previous operation if any before appending the operator.
function appendOperator(sign) {
	var displayedValue = document.getElementById('result').value;
	var removeNumbers = displayedValue.replace(/[\s\d\.]+/g, '');
	if (removeNumbers.length == 0) {
		firstNumber = parseFloat(displayedValue);
		if (sign == '=') {
			return;
		}
		operator = sign;
		document.getElementById('result').value += " " + sign + " ";
		resultDisplayed = false;
	} else if (removeNumbers.length == 1) {
		var numbers = displayedValue.replace(/\s/g, '').split(removeNumbers);
		if (numbers.length == 2) {
			if (numbers[0].length == 0)
				numbers[0] = 0;
			if (numbers[1].length == 0)
				numbers[1] = 0;
			firstNumber = parseFloat(numbers[0]);
			secondNumber = parseFloat(numbers[1]);
			operator = removeNumbers;
			performOperation(sign);
		} else if (numbers.length == 1) {
			if (numbers[0].length == 0)
				numbers[0] = 0;
			firstNumber = parseFloat(numbers[0]);
			if (sign == '=') {
				return;
			}
			operator = sign;
			displayedValue.replace(removeNumbers, sign);
			resultDisplayed = false;
		}
	} else {
		var tempOperator = removeNumbers.replace('-', '');
		var numbers = displayedValue.replace(/\s/g, '').split(tempOperator);
		if (numbers.length == 3) {
			if (numbers[0].length == 0) {
				firstNumber = parseFloat("-" + numbers[1]);
				secondNumber = parseFloat(numbers[2]);
				operator = tempOperator;
				performOperation(sign);
			}
		} else if (numbers.length == 2) {
			if (numbers[0].length == 0)
				numbers[0] = 0;
			if (numbers[1].length == 0)
				numbers[1] = 0;
			firstNumber = parseFloat(numbers[0]);
			secondNumber = parseFloat(numbers[1]);
			operator = tempOperator;
			performOperation(sign);
		} else if (numbers.length == 1) {
			if (numbers[0].length == 0)
				numbers[0] = 0;
			firstNumber = parseFloat(numbers[0]);
			if (sign == '=') {
				return;
			}
			operator = sign;
			displayedValue.replace(tempOperator, sign);
		}
	}
}
// performs the operation and displays the result
function performOperation(sign) {
	var result;
	switch(operator) {
		case '+':
			result = firstNumber + secondNumber;
			break;
		case '-':
			result = firstNumber - secondNumber;
			break;
		case '*':
			result = firstNumber * secondNumber;
			break;
		case '/':
			result = firstNumber / secondNumber;
			break;
	}
	var decimalPlaces;
	decimalPlaces = result.toString().split('.');
	if (decimalPlaces[1] && decimalPlaces[1].length > 8) {
		result = parseFloat(result).toFixed(8);
	}
	if (sign) {
		if (sign == '=') {
			document.getElementById('result').value = result
			resultDisplayed = true;
		} else {
			document.getElementById('result').value = result + " " + sign + " ";
			resultDisplayed = false;
		}
	} else {
		document.getElementById('result').value = result;
		resultDisplayed = true;
	}
}